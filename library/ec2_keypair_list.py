#!/usr/bin/python

DOCUMENTATION = '''
---
module: ec2-keypair-list
short_description: Outputs a list of the ec2 keypair and fingerprints
# ... snip ...
'''

EXAMPLES = '''
- name: Test ec2_keypair_list module
  ec2_keypair_list: 
     region: us-east-1
  register: ec2_result
'''

import boto3
import json

from ansible.module_utils.basic import *

def main():
    fields = {
       "region": {"required": True, "type": "str"}
    }

    module = AnsibleModule(argument_spec = fields)
    aws_region = module.params['region']
    ec2 = boto3.resource('ec2', region_name = aws_region)
    keypairs = ec2.meta.client.describe_key_pairs()
    response = keypairs
    module.exit_json(changed=False, meta=response)

if __name__ == '__main__':
    main()
